import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { MatButtonModule, MatDialogModule, MatIconModule, MatToolbarModule } from '@angular/material';
import { ToastrModule } from 'ngx-toastr';

import { environment } from 'environments/environment';
import { ROUTES } from './app.routes';

import { FuseModule } from './@fuse/fuse.module';
import {
  FuseProgressBarModule,
  FuseSidebarModule,
  FuseThemeOptionsModule
} from './@fuse/components';
import { FuseSharedModule } from './@fuse/shared.module';

import { fuseConfig } from './fuse-config';

import '../styles/styles.scss';
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { LayoutModule } from './layout/layout.module';
import { AppState } from './app.service';
import { LoginModule } from './main/login/login.module';
import { AuthGuard } from './main/guards/auth.guard';
import { HttpErrorInterceptor } from './exception/http-error.interceptor';
import { setAppInjector } from './app.injector';
import { ForgotPasswordModule } from './main/forgot-password/forgot-password.module';
import { ResetPasswordModule } from './main/reset-password/reset-password.module';
import { SignOnGuard } from './main/guards/sign-on.guard';
import { JwtConfig, jwtOptionsFactory } from './jwt.config';
import { UserService } from './main/user/user.service';
import { PermissionService } from './main/permission/permission.service';
import { DeleteDialogComponent } from './main/base/base-list/delete-dialog.component';
import { LocalizationInterceptor } from './interceptor/localization.interceptor';

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState,
  AuthGuard,
  SignOnGuard,
  JwtConfig,
  UserService,
  PermissionService,
  { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: LocalizationInterceptor, multi: true }
];

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    DeleteDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false,
      preloadingStrategy: PreloadAllModules
    }),

    TranslateModule.forRoot(),

    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [JwtConfig],
      },
    }),

    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      enableHtml: true,
      onActivateTick: true
    }),

    // Material moment date module
    MatMomentDateModule,
    MatDialogModule,
    MatToolbarModule,

    // Material
    MatButtonModule,
    MatIconModule,

    // Fuse modules
    FuseModule.forRoot(fuseConfig),
    FuseProgressBarModule,
    FuseSharedModule,
    FuseSidebarModule,
    FuseThemeOptionsModule,

    // App modules
    LayoutModule,
    LoginModule,
    ForgotPasswordModule,
    ResetPasswordModule,
  ],
  entryComponents: [
    DeleteDialogComponent,
  ],
  providers: [
    environment.ENV_PROVIDERS,
    APP_PROVIDERS
  ]
})
export class AppModule {
  constructor(
    public injector: Injector
  ) {
    setAppInjector(injector);
  }
}
