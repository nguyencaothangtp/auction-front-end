export function currencyToNumber(currency) {
  currency = currency + '';
  let result = currency.replace(/\./g, '');

  result = result.replace(/,/g, '');
  result = result.replace(/ VND/g, '');

  return result;
}

export function numberToCurrency(num, currency) {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ' ' + currency;
}
