import * as moment from 'moment';

export function getDateOnly(datetime) {
  return moment(datetime).format('YYYY-MM-DD');
}
