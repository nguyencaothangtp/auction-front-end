import { Injectable } from '@angular/core';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

export function jwtOptionsFactory(jwtConfig: JwtConfig) {
  return {
    tokenGetter,
    whitelistedDomains: jwtConfig.whitelistedDomains,
    blacklistedRoutes: jwtConfig.blacklistedRoutes,
    skipWhenExpired: true
  };
}

@Injectable({
  providedIn: 'root'
})
export class JwtConfig {
  public blacklistedRoutes: any;
  public whitelistedDomains: any;

  constructor() {
    this.blacklistedRoutes = [];

    this.whitelistedDomains = [
      new RegExp('[\s\S]*'),
    ];
  }
}
