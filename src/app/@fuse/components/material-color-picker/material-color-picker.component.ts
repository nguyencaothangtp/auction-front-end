import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  ViewEncapsulation
} from '@angular/core';

import { fuseAnimations } from '../../animations';
import { MatColors } from '../../mat-colors';

@Component({
  selector: 'fuse-material-color-picker',
  templateUrl: './material-color-picker.component.html',
  styleUrls: ['./material-color-picker.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class FuseMaterialColorPickerComponent implements OnChanges {
  public colors: any;
  public hues: string[];
  public selectedColor: any;
  public view: string;

  @Input()
  public selectedPalette: string;

  @Input()
  public selectedHue: string;

  @Input()
  public selectedFg: string;

  @Input()
  public value: any;

  @Output()
  public onValueChange: EventEmitter<any>;

  @Output()
  public selectedPaletteChange: EventEmitter<any>;

  @Output()
  public selectedHueChange: EventEmitter<any>;

  @Output()
  public selectedClassChange: EventEmitter<any>;

  @Output()
  public selectedBgChange: EventEmitter<any>;

  @Output()
  public selectedFgChange: EventEmitter<any>;

  // Private
  public _selectedClass: string;
  public _selectedBg: string;

  /**
   * Constructor
   */
  constructor() {
    // Set the defaults
    this.colors = MatColors.all;
    this.hues = ['50', '100', '200', '300', '400', '500', '600', '700', '800', '900', 'A100', 'A200', 'A400', 'A700'];
    this.selectedFg = '';
    this.selectedHue = '';
    this.selectedPalette = '';
    this.view = 'palettes';

    this.onValueChange = new EventEmitter();
    this.selectedPaletteChange = new EventEmitter();
    this.selectedHueChange = new EventEmitter();
    this.selectedClassChange = new EventEmitter();
    this.selectedBgChange = new EventEmitter();
    this.selectedFgChange = new EventEmitter();

    // Set the private defaults
    this._selectedClass = '';
    this._selectedBg = '';
  }

  /**
   * Selected class
   *
   * @param value
   */
  @Input()
  set selectedClass(value) {
    if (value && value !== '' && this._selectedClass !== value) {
      const color = value.split('-');
      if (color.length >= 5) {
        this.selectedPalette = color[1] + '-' + color[2];
        this.selectedHue = color[3];
      } else {
        this.selectedPalette = color[1];
        this.selectedHue = color[2];
      }
    }
    this._selectedClass = value;
  }

  get selectedClass(): string {
    return this._selectedClass;
  }

  /**
   * Selected bg
   *
   * @param value
   */
  @Input()
  set selectedBg(value) {
    if (value && value !== '' && this._selectedBg !== value) {
      for (const palette in this.colors) {
        if (!this.colors.hasOwnProperty(palette)) {
          continue;
        }

        for (const hue of this.hues) {
          if (this.colors[palette][hue] === value) {
            this.selectedPalette = palette;
            this.selectedHue = hue;
            break;
          }
        }
      }
    }
    this._selectedBg = value;
  }

  get selectedBg(): string {
    return this._selectedBg;
  }

  /**
   * On changes
   *
   * @param changes
   */
  public ngOnChanges(changes: any): void {
    if (changes.selectedBg && changes.selectedBg.currentValue === '' ||
      changes.selectedClass && changes.selectedClass.currentValue === '' ||
      changes.selectedPalette && changes.selectedPalette.currentValue === '') {
      this.removeColor();
      return;
    }
    if (changes.selectedPalette || changes.selectedHue || changes.selectedClass || changes.selectedBg) {
      this.updateSelectedColor();
    }
  }

  /**
   * Select palette
   *
   * @param palette
   */
  public selectPalette(palette): void {
    this.selectedPalette = palette;
    this.updateSelectedColor();
    this.view = 'hues';
  }

  /**
   * Select hue
   *
   * @param hue
   */
  public selectHue(hue): void {
    this.selectedHue = hue;
    this.updateSelectedColor();
  }

  /**
   * Remove color
   */
  public removeColor(): void {
    this.selectedPalette = '';
    this.selectedHue = '';
    this.updateSelectedColor();
    this.view = 'palettes';
  }

  /**
   * Update selected color
   */
  public updateSelectedColor(): void {
    setTimeout(() => {

      if (this.selectedColor && this.selectedPalette === this.selectedColor.palette && this.selectedHue === this.selectedColor.hue) {
        return;
      }

      if (this.selectedPalette !== '' && this.selectedHue !== '') {
        this.selectedBg = MatColors.getColor(this.selectedPalette)[this.selectedHue];
        this.selectedFg = MatColors.getColor(this.selectedPalette).contrast[this.selectedHue];
        this.selectedClass = 'mat-' + this.selectedPalette + '-' + this.selectedHue + '-bg';
      } else {
        this.selectedBg = '';
        this.selectedFg = '';
      }

      this.selectedColor = {
        palette: this.selectedPalette,
        hue: this.selectedHue,
        class: this.selectedClass,
        bg: this.selectedBg,
        fg: this.selectedFg
      };

      this.selectedPaletteChange.emit(this.selectedPalette);
      this.selectedHueChange.emit(this.selectedHue);
      this.selectedClassChange.emit(this.selectedClass);
      this.selectedBgChange.emit(this.selectedBg);
      this.selectedFgChange.emit(this.selectedFg);

      this.value = this.selectedColor;
      this.onValueChange.emit(this.selectedColor);
    });
  }

  /**
   * Go back to palette selection
   */
  public backToPaletteSelection(): void {
    this.view = 'palettes';
  }

  /**
   * On menu open
   */
  public onMenuOpen(): void {
    if (this.selectedPalette === '') {
      this.view = 'palettes';
    } else {
      this.view = 'hues';
    }
  }
}
