import { Component, HostBinding, Input } from '@angular/core';

import { FuseNavigationItem } from '../../../../types';
import { AuthService } from '../../../../../main/auth/auth.service';

@Component({
  selector: 'fuse-nav-vertical-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class FuseNavVerticalItemComponent {
  @HostBinding('class')
  public classes = 'nav-item';

  @Input()
  public item: FuseNavigationItem;

  /**
   * Constructor
   */
  constructor(
    public authService: AuthService
  ) {
    //
  }

  public isAccessible(permissionName: string): boolean {
    return this.authService.isAccessible(permissionName);
  }
}
