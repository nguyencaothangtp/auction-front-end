import { Component, HostBinding, Input } from '@angular/core';

import { FuseNavigationItem } from '../../../../types';

@Component({
  selector: 'fuse-nav-vertical-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class FuseNavVerticalGroupComponent {
  @HostBinding('class')
  public classes = 'nav-group nav-item';

  @Input()
  public item: FuseNavigationItem;

  /**
   * Constructor
   */
  constructor() {
    // empty
  }
}
