import { Component, HostBinding, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '../../../../animations';
import { FuseConfigService } from '../../../../services/config.service';

@Component({
  selector: 'fuse-nav-horizontal-collapsable',
  templateUrl: './collapsable.component.html',
  styleUrls: ['./collapsable.component.scss'],
  animations: fuseAnimations
})
export class FuseNavHorizontalCollapsableComponent implements OnInit, OnDestroy {
  public fuseConfig: any;
  public isOpen = false;

  @HostBinding('class')
  public classes = 'nav-collapsable nav-item';

  @Input()
  public item: any;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _fuseConfigService: FuseConfigService
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  /**
   * On init
   */
  public ngOnInit(): void {
    // Subscribe to config changes
    this._fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (config) => {
          this.fuseConfig = config;
        }
      );
  }

  /**
   * On destroy
   */
  public ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  /**
   * Open
   */
  @HostListener('mouseenter')
  public open(): void {
    this.isOpen = true;
  }

  /**
   * Close
   */
  @HostListener('mouseleave')
  public close(): void {
    this.isOpen = false;
  }
}
