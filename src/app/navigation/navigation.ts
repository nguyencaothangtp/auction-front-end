import { FuseNavigation } from '../@fuse/types';

export const navigation: FuseNavigation[] = [{
  id: 'applications',
  title: 'Applications',
  translate: 'NAV.APPLICATIONS',
  type: 'group',
  children: [
    {
      icon: 'all_inbox',
      id: 'products',
      title: 'Products',
      translate: 'NAV.PRODUCT.TITLE',
      type: 'item',
      url: '/products',
    },
    {
      icon: 'assignment',
      id: 'profile',
      title: 'Configs',
      translate: 'NAV.CONFIG.TITLE',
      type: 'item',
      url: '/profile',
    },
  ]
}];
