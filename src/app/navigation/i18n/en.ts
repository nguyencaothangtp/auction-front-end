export const locale = {
  lang: 'en',
  data: {
    NAV: {
      APPLICATIONS: 'Applications',
      PRODUCT: {
        TITLE: 'Product',
      },
      CONFIG: {
        TITLE: 'Config',
      },
    },
  }
};
