import { Routes } from '@angular/router';

import { AuthGuard } from './main/guards/auth.guard';
import { LoginComponent } from './main/login/login.component';
import { ForgotPasswordComponent } from './main/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './main/reset-password/reset-password.component';
import { SignOnGuard } from './main/guards/sign-on.guard';

export const ROUTES: Routes = [
  { path: '', redirectTo: '/products', pathMatch: 'full' },

  {
    path: '',
    loadChildren: './main/main.module#MainModule',
    canActivate: [ AuthGuard ]
  },

  { path: 'login', component: LoginComponent, canActivate: [ SignOnGuard ] },
  { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [ SignOnGuard ] },
  { path: 'reset-password/:token', component: ResetPasswordComponent, canActivate: [ SignOnGuard ] },

  {
    path: 'errors',
    children: [
      { path: 'error-404', loadChildren: './exception/errors/404/error-404.module#Error404Module' },
    ]
  },

  { path: '**', redirectTo: 'errors/error-404' },
];
