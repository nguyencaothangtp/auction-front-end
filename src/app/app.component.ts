import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Platform } from '@angular/cdk/platform';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from './@fuse/services/config.service';
import { FuseNavigationService } from './@fuse/components/navigation/navigation.service';
import { FuseSidebarService } from './@fuse/components/sidebar/sidebar.service';
import { FuseSplashScreenService } from './@fuse/services/splash-screen.service';
import { FuseTranslationLoaderService } from './@fuse/services/translation-loader.service';

import { locale as english } from 'app/navigation/i18n/en';
import { locale as vietnamese } from 'app/navigation/i18n/vi';
import { navigation } from 'app/navigation/navigation';
import { AppState } from './app.service';

export const ROOT_SELECTOR = 'app';

const languages = [english, vietnamese];

@Component({
  selector: ROOT_SELECTOR,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public fuseConfig: any;
  public navigation: any;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {AppState} appState
   * @param {Router} router
   * @param {DOCUMENT} document
   * @param {FuseConfigService} _fuseConfigService
   * @param {FuseNavigationService} _fuseNavigationService
   * @param {FuseSidebarService} _fuseSidebarService
   * @param {FuseSplashScreenService} _fuseSplashScreenService
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   * @param {Platform} _platform
   * @param {TranslateService} _translateService
   */
  constructor(
    public appState: AppState,
    public router: Router,
    @Inject(DOCUMENT) private document: any,
    private _fuseConfigService: FuseConfigService,
    private _fuseNavigationService: FuseNavigationService,
    private _fuseSidebarService: FuseSidebarService,
    private _fuseSplashScreenService: FuseSplashScreenService,
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _translateService: TranslateService,
    private _platform: Platform
  ) {
    const defaultLanguage = localStorage.getItem('currentLanguage') || 'vi';

    // Get default navigation
    this.navigation = navigation;

    // Register the navigation to the service
    this._fuseNavigationService.register('main', this.navigation);

    // Set the main navigation as our current navigation
    this._fuseNavigationService.setCurrentNavigation('main');

    // Add languages
    this._translateService.addLangs(['en', 'vi']);

    // Set the default language
    this._translateService.setDefaultLang(defaultLanguage);

    // Set the navigation translations
    this._fuseTranslationLoaderService.loadTranslations(...languages);

    // Use a language
    this._translateService.use(defaultLanguage);

    // Add is-mobile class to the body if the platform is mobile
    if (this._platform.ANDROID || this._platform.IOS) {
      this.document.body.classList.add('is-mobile');
    }

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  /**
   * On init
   */
  public async ngOnInit() {
    // Subscribe to config changes
    this._fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        this.fuseConfig = config;

        if (this.fuseConfig.layout.width === 'boxed') {
          this.document.body.classList.add('boxed');
        } else {
          this.document.body.classList.remove('boxed');
        }
      });

    // check root to redirect because redirectTo not working
    if (location.pathname === '/') {
      this.router.navigate(['/products']);
    }
  }

  /**
   * On destroy
   */
  public ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
