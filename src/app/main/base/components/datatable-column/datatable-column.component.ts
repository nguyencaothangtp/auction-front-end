import { Component, Input } from '@angular/core';

import { fuseAnimations } from '../../../../@fuse/animations';

@Component({
  selector: 'datatable-column',
  templateUrl: './datatable-column.component.html',
  // styleUrls: [ './datatable-column.component.scss' ],
  animations: fuseAnimations
})
export class DatatableColumnComponent {
  @Input('column')
  public column: any = {};
}
