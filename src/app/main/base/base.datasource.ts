import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { BaseService } from './base.service';

export class BaseDatasource implements DataSource<any> {
  public dataSubject = new BehaviorSubject<any[]>([]);
  public loadingSubject = new BehaviorSubject<boolean>(false);
  public response: any;

  public isLoading = this.loadingSubject.asObservable();

  constructor(
    public baseService: BaseService
  ) {
    // empty
  }

  public connect(collectionViewer: CollectionViewer): Observable<any[]> {
    return this.dataSubject.asObservable();
  }

  public disconnect(collectionViewer: CollectionViewer): void {
    this.dataSubject.complete();
    this.loadingSubject.complete();
  }

  public loadData(data) {
    this.loadingSubject.next(true);

    this.baseService.list(data).pipe(
      finalize(() => this.loadingSubject.next(false))

    ).subscribe((response: any) => {
      this.response = response;
      this.dataSubject.next(response.data);
    });
  }
}
