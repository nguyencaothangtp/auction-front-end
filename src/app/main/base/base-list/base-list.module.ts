import { NgModule } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

import {
  DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE,
  MatAutocompleteModule,
  MatButtonModule,
  MatChipsModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule, MatProgressSpinnerModule,
  MatRippleModule,
  MatSelectModule, MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';

import {
  DateAdapter as satDateAdapter,
  MAT_DATE_FORMATS as SAT_MAT_DATE_FORMATS,
  MAT_DATE_LOCALE as SAT_MAT_DATE_LOCALE,
  SatDatepickerModule, SatNativeDateModule
} from 'saturn-datepicker';

import * as _moment from 'moment';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

import { FuseSharedModule } from '../../../@fuse/shared.module';
import { FuseWidgetModule } from '../../../@fuse/components';
import { DatatableColumnComponent } from '../components/datatable-column/datatable-column.component';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  declarations: [
    DatatableColumnComponent,
  ],
  imports: [
    MatButtonModule,
    MatChipsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    NgxMatSelectSearchModule,
    SatDatepickerModule,
    SatNativeDateModule,

    FuseSharedModule,
    FuseWidgetModule,
  ],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: satDateAdapter, useClass: MomentDateAdapter, deps: [SAT_MAT_DATE_LOCALE] },
    { provide: SAT_MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
  exports: [
    MatButtonModule,
    MatChipsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    NgxMatSelectSearchModule,
    MatDatepickerModule,
    SatDatepickerModule,
    SatNativeDateModule,

    FuseSharedModule,
    FuseWidgetModule,
    DatatableColumnComponent
  ]
})
export class BaseListModule {
}
