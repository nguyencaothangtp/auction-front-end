import { AfterViewInit, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';

import { FuseTranslationLoaderService } from '../../../@fuse/services/translation-loader.service';
import { FuseProgressBarService } from '../../../@fuse/components/progress-bar/progress-bar.service';

import { BaseService } from '../base.service';
import { BaseDatasource } from '../base.datasource';
import { merge, Subject } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';
import { DeleteDialogComponent } from './delete-dialog.component';
import { AppInjector } from '../../../app.injector';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../auth/auth.service';
import * as moment from 'moment';

export class BaseListComponent implements OnInit, AfterViewInit, OnDestroy {
  public isLoading: boolean = false;

  @ViewChild(MatPaginator)
  public paginator: MatPaginator;

  @ViewChild(MatSort)
  public sort: MatSort;

  public dataSource: any = new MatTableDataSource<any>([]);

  public pageSizeOptions = [10, 25, 50, 100];
  public startPage = 0;
  public perPage = this.pageSizeOptions[0];
  public filters: any = [];
  public joins: any = [];
  public defaultSort: any = [];
  public filterApply: Subject<any> = new Subject<any>();
  public displayedColumns: any = [];

  constructor(
    public fuseTranslationLoaderService: FuseTranslationLoaderService,
    public baseService: BaseService,
    public fuseProgressBarService: FuseProgressBarService,
    public languages: any,
    public matSnackBar: MatSnackBar,
    public authService: AuthService
  ) {
    this.fuseTranslationLoaderService.loadTranslations(...languages);
  }

  public ngOnInit() {
    this.dataSource = new BaseDatasource(this.baseService);
    this.mapFiltersValue();
    this.mapPaginationValue();

    this.loadDataPage(this.filters);

    this.dataSource.isLoading.subscribe((isLoading) => {
      if (isLoading) {
        this.fuseProgressBarService.show();
      } else {
        this.fuseProgressBarService.hide();
      }
    });

    this.filterApply.pipe(
      debounceTime(500)
    ).subscribe((filters) => {
      this.loadDataPage(filters);
    });

    this.translateDisplayColumn();
  }

  public ngOnDestroy() {
    this.filterApply.complete();
    this.filterApply.unsubscribe();
  }

  public ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadDataPage())
      )
      .subscribe();
  }

  public mapPaginationValue() {
    const pagination = JSON.parse(localStorage.getItem(this.baseService.pathApi + '-pagination')) || {};

    this.paginator.pageIndex = pagination.page - 1;
    this.paginator.pageSize = pagination.per_page;
  }

  public mapFiltersValue() {
    this.displayedColumns = this.displayedColumns.map((col) => {
      col.value = '';
      return col;
    });

    this.filters = JSON.parse(localStorage.getItem(this.baseService.pathApi + '-filters')) || [];

    for (const filter of this.filters) {
      const column = this.displayedColumns.find((col) => {
        return col.key === filter.key || col.filter_key === filter.key;
      });

      if (column) {
        const operator = column.operator || 'like';
        let val = filter.value;

        switch (operator) {
          case 'eq':
            break;

          case 'like':
            // remove first, last character '*'
            val = val.substr(1);
            val = val.slice(0, -1);
            break;

          case 'range':
            // EXAMPLE VALUE: 'ge:2019-07-01:and:le:2019-07-25'
            val = filter.operator;

            val = val.replace('ge:', '');
            val = val.split(':and:le:');

            const endDate = new Date(val[1]);
            endDate.setDate(endDate.getDate() - 1);

            val = {
              begin: new Date(val[0]),
              end: endDate
            };
            break;
        }

        column.value = val;
      }
    }
  }

  public filterColumn(event, column) {
    let value;

    if (event && event.target) {
      value = event.target.value;
    } else {
      value = event;
    }

    // remove filter
    this.filters = this.filters.filter((e) => e.key !== (column.filter_key || column.key));

    // re-add filter
    if (value) {
      this.filters.push(this.prepareFilterColumn(column, value));
    }

    // store history filter
    localStorage.setItem(this.baseService.pathApi + '-filters', JSON.stringify(this.filters));

    // set pagination index to 0
    this.paginator.pageIndex = 0;

    // store pagination filter
    const data: any = {
      page: this.paginator.pageIndex + 1,
      per_page: this.paginator.pageSize || this.perPage
    };

    localStorage.setItem(this.baseService.pathApi + '-pagination', JSON.stringify(data));

    this.filterApply.next(this.filters);
  }

  public prepareFilterColumn(column, value) {
    let operator = column.operator || 'like';
    let val = '';

    switch (operator) {
      case 'eq':
        val = value;
        break;

      case 'like':
        val = '*' + value + '*';
        break;

      case 'range':
        const begin = moment(value.begin).format('YYYY-MM-DD');
        const end = moment(value.end).add(1, 'day').format('YYYY-MM-DD');
        operator = 'ge:' + begin + ':and:le:' + end;
        val = '';
        break;
    }

    column.filter_key = column.filter_key || column.key;

    return { key: column.filter_key, operator, value: val };
  }

  public getFilterColumns(displayedColumns) {
    return displayedColumns.map((column) => {
      return column.key + '-filter';
    });
  }

  public loadDataPage(filters: any = null) {
    this.dataSource.loadData(this.getJoinedFilters(filters));
  }

  public getJoinedFilters(filters: any = null) {
    const data: any = {
      page: this.paginator.pageIndex + 1,
      per_page: this.paginator.pageSize || this.perPage
    };

    localStorage.setItem(this.baseService.pathApi + '-pagination', JSON.stringify(data));

    if (this.sort.direction) {
      data[`order[${this.sort.active}]`] = this.sort.direction;

    } else if (this.defaultSort) {
      for (const sort of this.defaultSort) {
        data[sort.key] = sort.value;
      }
    }

    if (filters) {
      for (const filter of filters) {
        data[`filter[${filter.key}]`] = filter.operator + ':' + filter.value;
      }
    }

    if (this.joins) {
      for (const join of this.joins) {
        data[join.key] = join.value;
      }
    }

    return data;
  }

  public delete(event, row) {
    event.stopPropagation();

    const dialog = AppInjector.get(MatDialog);

    const dialogRef = dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: row
    });

    dialogRef.afterClosed().subscribe(async (isDelete) => {
      if (isDelete) {
        this.isLoading = true;

        const response = await this.baseService.delete(row.id).toPromise();

        this.isLoading = false;

        if (response) {
          // Show the success message
          this.matSnackBar.open('Record deleted', 'OK', {
            verticalPosition: 'top',
            duration: 2000,
          });

          this.loadDataPage();
        }
      }
    });
  }

  public translateDisplayColumn() {
    const translateService = AppInjector.get(TranslateService);
    for (const column of this.displayedColumns) {
      translateService.stream(column.title).subscribe((res: string) => {
        column.title = res;
      });
    }
  }

  public isAccessible(permissionName: string): boolean {
    return this.authService.isAccessible(permissionName);
  }

}
