import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs';

import { AppConfig } from '../../app.config';
import { AuthService } from '../auth/auth.service';
import { AppState } from '../../app.service';
import { AppInjector } from '../../app.injector';

@Injectable()
export abstract class BaseService {
  public abstract pathApi;

  public data: any;

  protected constructor(
    public http: HttpClient,
    public authService: AuthService,
  ) {
    // empty
  }

  public list(data, isCache = false) {
    if (!this.pathApi) {
      return new Observable<any>((subscriber) => subscriber.unsubscribe());
    }

    const table = this.pathApi.replace('/', '');

    const permissions = [
      'list ' + table,
    ];

    if (!this.authService.isAccessible(permissions)) {
      return new Observable<any>((subscriber) => subscriber.unsubscribe());
    }

    if (this.authService.isAccessible('list ' + table)) {
      delete data['filter[created_id]'];
    }

    if (isCache) {
      if (this.data) {
        return new Observable<any>((subscriber) => {
          subscriber.next(this.data);
          subscriber.complete();
        });

      } else {
        return new Observable<any>((subscriber) => {
          this.http.get(AppConfig.api_url + this.pathApi, { params: data }).subscribe((response) => {
            this.data = response;
            subscriber.next(this.data);
            subscriber.complete();
          });
        });
      }
    }

    return this.http.get(AppConfig.api_url + this.pathApi, { params: data });
  }

  public get(id) {
    if (!this.pathApi) {
      return new Observable<any>((subscriber) => subscriber.unsubscribe());
    }

    return this.http.get(AppConfig.api_url + this.pathApi + `/${id}`);
  }

  public create(data) {
    if (!this.pathApi) {
      return new Observable<any>((subscriber) => subscriber.unsubscribe());
    }

    return this.http.post(AppConfig.api_url + this.pathApi, data);
  }

  public update(data) {
    if (!this.pathApi) {
      return new Observable<any>((subscriber) => subscriber.unsubscribe());
    }

    let id;

    if (data instanceof FormData) {
      id = data.get('id');
      data.append('_method', 'PUT');
      return this.http.post(AppConfig.api_url + this.pathApi + `/${id}`, data);

    } else {
      id = data.id;
      delete data.id;
      return this.http.put(AppConfig.api_url + this.pathApi + `/${id}`, data);
    }
  }

  public delete(id) {
    if (!this.pathApi) {
      return new Observable<any>((subscriber) => subscriber.unsubscribe());
    }

    return this.http.delete(AppConfig.api_url + this.pathApi + `/${id}`);
  }
}
