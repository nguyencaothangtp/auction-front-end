import { FormGroup } from '@angular/forms';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import * as cloneDeep from 'clone-deep';

import { FuseProgressBarService } from '../../../@fuse/components/progress-bar/progress-bar.service';

import { BaseService } from '../base.service';
import { AuthService } from '../../auth/auth.service';

export abstract class BaseDetailComponent implements OnInit {
  public model: any = {};
  public pageType: string;
  public modelForm: FormGroup;
  public isLoading: boolean = false;
  public abstract path: string;
  public id: any;

  constructor(
    public activatedRoute: ActivatedRoute,
    public baseService: BaseService,
    public router: Router,
    public matSnackBar: MatSnackBar,
    public fuseProgressBarService: FuseProgressBarService,
    public authService: AuthService
  ) {
    // empty
  }

  public async ngOnInit() {
    this.id = this.activatedRoute.snapshot.params.id;

    if (!this.id) {
      this.pageType = 'new';
      this.model = {};
    }

    this.modelForm = this.buildForm();

    if (this.id) {
      this.fuseProgressBarService.show();

      this.model = await this.baseService.get(this.id).toPromise();
      this.model = this.model.data;

      if (!this.model) {
        return this.router.navigate([this.path]);
      }

      this.fuseProgressBarService.hide();

      this.pageType = 'edit';
      this.modelForm = this.buildForm();
    }
  }

  public abstract buildForm();

  public compareFunction(model1, model2) {
    if (typeof model1 === 'object' && typeof model2 === 'object') {
      return model1.id === model2.id;
    } else {
      return model1 === model2;
    }
  }

  public async update() {
    this.isLoading = true;

    const response = await this.baseService.update(this.prepareSubmitData(this.modelForm.value)).toPromise();

    this.isLoading = false;

    if (response) {
      // Show the success message
      this.matSnackBar.open('Information saved', 'OK', {
        verticalPosition: 'top',
        duration: 2000,
      });
    }
  }

  public async create() {
    this.isLoading = true;

    const response = await this.baseService.create(this.prepareSubmitData(this.modelForm.value)).toPromise();

    this.isLoading = false;

    if (response) {
      // Show the success message
      this.matSnackBar.open('Information saved', 'OK', {
        verticalPosition: 'top',
        duration: 2000,
      });

      // Change the location with new one
      this.router.navigate([ this.path + '/edit/' + response.id]);
    }
  }

  public prepareSubmitData(value) {
    const data = cloneDeep(value);

    if (this.pageType === 'edit') {
      data.id = this.model.id;
    }

    return data;
  }

  public isAccessible(permissionName: string): boolean {
    return this.authService.isAccessible(permissionName);
  }

  public isBelongToBranch(products): boolean {
    return this.authService.isBelongToBranch(products);
  }
}
