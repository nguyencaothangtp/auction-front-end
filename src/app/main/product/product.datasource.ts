import { ProductService } from './product.service';
import { BaseDatasource } from '../base/base.datasource';

export class ProductDatasource extends BaseDatasource {
  constructor(
    public productService: ProductService
  ) {
    super(productService);
  }
}
