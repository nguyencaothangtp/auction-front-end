import { AfterViewInit, Component, OnInit } from '@angular/core';

import { fuseAnimations } from '../../@fuse/animations';
import { FuseProgressBarService } from '../../@fuse/components/progress-bar/progress-bar.service';
import { FuseTranslationLoaderService } from '../../@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as vietnamese } from './i18n/vi';
import { ProductService } from './product.service';
import { BaseListComponent } from '../base/base-list/base-list.component';
import { Colors } from '../../app.config';
import { MatSnackBar } from '@angular/material';
import { AuthService } from '../auth/auth.service';

const languages = [ english, vietnamese ];

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: [ './product-list.component.scss' ],
  animations: fuseAnimations
})
export class ProductListComponent extends BaseListComponent implements OnInit, AfterViewInit {
  public mobileColumns: any = [
    'name', 'initial_price'
  ];

  public desktopColumns: any = [
    'description'
  ];

  public activeOptions = [
    { key: 'All', value: null },
    { key: 'Open', value: 'Open' },
    { key: 'Closed', value: 'Closed' }
  ];

  public displayedColumns: any = [
    { title: 'Name', key: 'name', filter: true, operator: 'like' },
    { title: 'Description', key: 'description', filter: true, operator: 'like' },
    { title: 'Initial Price', key: 'initial_price', filter: true, operator: 'like' },
    { title: 'Status', key: 'status', filter: true, operator: 'eq', options: this.activeOptions },
  ];

  public colors = Colors;

  constructor(
    public fuseTranslationLoaderService: FuseTranslationLoaderService,
    public productService: ProductService,
    public fuseProgressBarService: FuseProgressBarService,
    public matSnackBar: MatSnackBar,
    public authService: AuthService
  ) {
    super(fuseTranslationLoaderService, productService, fuseProgressBarService, languages, matSnackBar, authService);
  }
}
