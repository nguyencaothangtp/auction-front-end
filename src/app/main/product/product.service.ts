import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from '../base/base.service';
import { AuthService } from '../auth/auth.service';
import {Observable} from 'rxjs';
import {AppConfig} from '../../app.config';

@Injectable()
export class ProductService extends BaseService {

  public pathApi: string = '/products';
  public biddingApi: string = '/bidding';

  constructor(
    public httpClient: HttpClient,
    public authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public bid(amount, productId) {
    return this.http.post(AppConfig.api_url + this.biddingApi + `/${productId}`, {amount});
  }

  public getAutoBidConfig(productId): Observable<any> {
    return this.http.get(AppConfig.api_url + this.biddingApi + `/${productId}/config`);
  }

  public getHistory(productId): Observable<any> {
    return this.http.get(AppConfig.api_url + this.biddingApi + `/${productId}/history`);
  }

  public turnOnAutoBidConfig(productId) {
    return this.http.get(AppConfig.api_url + this.biddingApi + `/${productId}/turn-auto-bid-on`);
  }

  public turnOffAutoBidConfig(productId) {
    return this.http.get(AppConfig.api_url + this.biddingApi + `/${productId}/turn-auto-bid-off`);
  }
}
