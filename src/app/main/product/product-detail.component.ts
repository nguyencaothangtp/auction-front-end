import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

import { fuseAnimations } from '../../@fuse/animations';
import { FuseProgressBarService } from '../../@fuse/components/progress-bar/progress-bar.service';

import { ProductService } from './product.service';
import { BaseDetailComponent } from '../base/base-detail/base-detail.component';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ProductDetailComponent extends BaseDetailComponent implements OnInit {
  public path: string = '/products';
  public autoBidConfig;
  public historyData;
  public historyDisplayedColumns: string[] = ['name', 'bid', 'updated_at'];

  constructor(
    public activatedRoute: ActivatedRoute,
    public productService: ProductService,
    public router: Router,
    public matSnackBar: MatSnackBar,
    public fuseProgressBarService: FuseProgressBarService,
    public formBuilder: FormBuilder,
    public authService: AuthService
  ) {
    super(activatedRoute, productService, router, matSnackBar, fuseProgressBarService, authService);
  }

  public buildForm(): FormGroup {
    return this.formBuilder.group({
      name: [this.model.name],
      description: [this.model.description],
      amount: [0, [Validators.required, Validators.min(0)]],
      is_active: [this.model.is_active || false, [Validators.required]]
    });
  }

  public async bid() {
    this.isLoading = true;

    const response = await this.productService.bid(this.modelForm.value.amount, this.id).toPromise();

    this.isLoading = false;

    if (response) {
      // Show the success message
      this.matSnackBar.open('Information saved', 'OK', {
        verticalPosition: 'top',
        duration: 2000,
      });
    }
  }

  public async loadData(event) {
    this.isLoading = true;
    switch (event.index) {
      case 1: // Auto-bid tab
        if (this.autoBidConfig == null) {
          const response = await this.productService.getAutoBidConfig(this.id).toPromise();
          if (response) {
            this.autoBidConfig = response.data.status;
          }
        }
        break;
      case 2: // History tab
        if (this.autoBidConfig == null) {
          const response = await this.productService.getHistory(this.id).toPromise();
          if (response) {
            this.historyData = response.data;
          }
        }

        break;
    }
    this.isLoading = false;
  }

  public async changeAutoBidConfig(event) {
    this.isLoading = true;
    let response;
    switch (event.checked) {
      case true:
        response = await this.productService.turnOnAutoBidConfig(this.id).toPromise();
        break;
      case false:
        response = await this.productService.turnOffAutoBidConfig(this.id).toPromise();
        break;
    }

    if (response) {
      this.matSnackBar.open('Information saved', 'OK', {
        verticalPosition: 'top',
        duration: 2000,
      });
    }

    this.isLoading = false;
  }
}
