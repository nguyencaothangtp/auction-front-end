import { NgModule } from '@angular/core';

import { BaseListModule } from '../base/base-list/base-list.module';
import { ProductRoutes } from './product.routes';
import { SharedModule } from '../shared.module';
import { ProductListComponent } from './product-list.component';
import { ProductDetailComponent } from './product-detail.component';
import { ProductService } from './product.service';

@NgModule({
  declarations: [
    ProductListComponent,
    ProductDetailComponent
  ],
  imports: [
    ProductRoutes,
    BaseListModule,
    SharedModule,
  ],
  providers: [
    ProductService
  ]
})
export class ProductModule {
}
