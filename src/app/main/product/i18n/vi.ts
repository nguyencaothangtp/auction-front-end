export const locale = {
  lang: 'vi',
  data: {
    'Products': 'Sản phẩm',
    'Save': 'Lưu',
    'Add new product': 'Thêm sản phẩm',
    'Product Detail' : 'Thông tin sản phẩm',
    'Information' : 'Thông tin',
    'Name': 'Tên',
    'Description': 'Mô tả',
    'Status': 'Trạng thái',
    'Open': 'Mở',
    'Closed': 'Đóng',
    'Initial Price': 'Giá khởi điểm',
    'Amount': 'Giá',
    'Auto-bid': 'Tự động bid',
    'History': 'Lịch sử',
    'Actions': 'Thao tác',
    'Active': 'Kích hoạt',
    'Filter': 'Lọc',
    'Inactive': 'Huỷ bỏ',
    'All': 'Tất cả'
  }
};
