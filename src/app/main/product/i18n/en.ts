export const locale = {
  lang: 'en',
  data: {
    'Products': 'Products',
    'Save': 'Save',
    'Add new product': 'Add new product',
    'Product Detail': 'Product Detail',
    'Information' : 'Information',
    'Name': 'Name',
    'Description': 'Description',
    'Status': 'Status',
    'Open': 'Open',
    'Closed': 'Closed',
    'Amount': 'Amount',
    'Auto-bid': 'Auto-bid',
    'History': 'History',
    'Initial Price': 'Initial Price',
    'Active': 'Active',
    'Actions': 'Actions',
    'Filter': 'Filter',
    'Inactive': 'Inactive',
    'All': 'All'
  }
};
