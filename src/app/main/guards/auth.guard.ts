import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AppState } from '../../app.service';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    public appState: AppState,
    public router: Router,
    public authService: AuthService,
  ) {
    // empty
  }

  public async canActivate() {
    const accessToken = localStorage.getItem('access_token');

    if (this.appState.state.user) {
      return true;
    }

    if (!this.appState.state.user && accessToken) {
      const response: any = await this.authService.profile();

      if (response) {
        this.appState.state.user = response.data;
        return true;
      }

      localStorage.removeItem('access_token');
    }

    this.router.navigate([ '/login' ]);
    return false;
  }
}
