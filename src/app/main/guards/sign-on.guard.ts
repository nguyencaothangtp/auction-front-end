import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AppState } from '../../app.service';

@Injectable()
export class SignOnGuard implements CanActivate {
  constructor(
    public appState: AppState,
    public router: Router,
  ) {
    // empty
  }

  public async canActivate() {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      this.router.navigate(['products']);
      return false;
    }

    return true;
  }
}
