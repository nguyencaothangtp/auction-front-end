import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from '../base/base.service';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class PermissionService extends BaseService {
  public pathApi: string = '/permissions';

  constructor(
    public httpClient: HttpClient,
    public authService: AuthService
  ) {
    super(httpClient, authService);
  }
}
