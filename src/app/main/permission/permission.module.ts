import { NgModule } from '@angular/core';

import { BaseListModule } from '../base/base-list/base-list.module';
import { PermissionRoutes } from './permission.routes';
import { SharedModule } from '../shared.module';
import { PermissionListComponent } from './permission-list.component';
import { PermissionsDetailComponent } from './permission-detail.component';

@NgModule({
  declarations: [
    PermissionListComponent,
    PermissionsDetailComponent
  ],
  imports: [
    PermissionRoutes,
    BaseListModule,
    SharedModule,
  ],
  providers: [
  ]
})
export class PermissionModule {
}
