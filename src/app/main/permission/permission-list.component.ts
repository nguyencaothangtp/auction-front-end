import { AfterViewInit, Component, OnInit } from '@angular/core';

import { fuseAnimations } from '../../@fuse/animations';
import { FuseProgressBarService } from '../../@fuse/components/progress-bar/progress-bar.service';
import { FuseTranslationLoaderService } from '../../@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as vietnamese } from './i18n/vi';
import { BaseListComponent } from '../base/base-list/base-list.component';
import { Colors } from '../../app.config';
import { PermissionService } from './permission.service';
import { MatSnackBar } from '@angular/material';
import { AuthService } from '../auth/auth.service';

const languages = [ english, vietnamese ];

@Component({
  selector: 'permission-list',
  templateUrl: './permission-list.component.html',
  styleUrls: [ './permission-list.component.scss' ],
  animations: fuseAnimations
})
export class PermissionListComponent extends BaseListComponent implements OnInit, AfterViewInit {
  public displayedColumns: any = [
    { title: 'ID', key: 'id' },
    { title: 'Name', key: 'name' },
    { title: 'Actions', key: 'actions' }
  ];

  public colors = Colors;

  constructor(
    public fuseTranslationLoaderService: FuseTranslationLoaderService,
    public permissionService: PermissionService,
    public fuseProgressBarService: FuseProgressBarService,
    public matSnackBar: MatSnackBar,
    public authService: AuthService
  ) {
    super(fuseTranslationLoaderService, permissionService, fuseProgressBarService, languages, matSnackBar, authService);
  }
}
