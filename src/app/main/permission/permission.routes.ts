import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PermissionListComponent } from './permission-list.component';
import { PermissionsDetailComponent } from './permission-detail.component';

const routes: Routes = [
  {
    path: '',
    component: PermissionListComponent,
  },
  {
    path: 'new',
    component: PermissionsDetailComponent,
  },
  {
    path: 'edit/:id',
    component: PermissionsDetailComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class PermissionRoutes {
}
