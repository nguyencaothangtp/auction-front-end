import { BaseDatasource } from '../base/base.datasource';
import { PermissionService } from './permission.service';

export class PermissionDatasource extends BaseDatasource {
  constructor(
    public permissionService: PermissionService
  ) {
    super(permissionService);
  }
}
