import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

import { fuseAnimations } from '../../@fuse/animations';
import { FuseProgressBarService } from '../../@fuse/components/progress-bar/progress-bar.service';

import { BaseDetailComponent } from '../base/base-detail/base-detail.component';
import { PermissionService } from './permission.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'permission-detail',
  templateUrl: './permission-detail.component.html',
  styleUrls: [ './permission-detail.component.scss' ],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class PermissionsDetailComponent extends BaseDetailComponent implements OnInit {
  public path: string = '/permissions';

  constructor(
    public activatedRoute: ActivatedRoute,
    public permissionService: PermissionService,
    public router: Router,
    public matSnackBar: MatSnackBar,
    public fuseProgressBarService: FuseProgressBarService,
    public formBuilder: FormBuilder,
    public authService: AuthService
  ) {
    super(activatedRoute, permissionService, router, matSnackBar, fuseProgressBarService, authService);
  }

  public buildForm(): FormGroup {
    const form = this.formBuilder.group({
      name: [ this.model.name, [ Validators.required ] ]
    });

    return form;
  }
}
