import { NgModule } from '@angular/core';

import { LaddaModule } from 'angular2-ladda';
import { NgPipesModule } from 'ngx-pipes';
import { TranslateModule } from '@ngx-translate/core';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  imports: [
    LaddaModule.forRoot({
      style: 'expand-left',
      spinnerColor: '#cccccc',
    }),
    NgPipesModule,
    TranslateModule
  ],
  providers: [],
  declarations: [],
  exports: [
    LaddaModule,
    NgPipesModule,
    TranslateModule,
    NgxCurrencyModule
  ]
})

export class SharedModule {
}
