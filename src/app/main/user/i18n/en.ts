export const locale = {
  lang: 'en',
  data: {
    'Users': 'Users',
    'Save': 'Save',
    'Add new user': 'Add new user',
    'User Detail' : 'User detail',
    'User type' : 'User type',
    'Basic Info': 'Basic Info',
    'Roles' : 'Roles',
    'Products': 'Products',
    'Username': 'Username',
    'Name': 'Name',
    'Code': 'Code',
    'Email': 'Email',
    'Password': 'Password',
    'Password (Confirm)': 'Password (Confirm)',
    'Active': 'Active',
    'Actions': 'Actions',
    'Filter': 'Filter',
    'Inactive': 'Inactive',
    'All': 'All',
    'Consultant': 'Consultant',
    'Personal Trainer': 'Personal Trainer',
    'Receptionist': 'Receptionist',
    'Trainer (Booking)': 'Trainer (Booking)',
    'Manager': 'Manager'
  }
};
