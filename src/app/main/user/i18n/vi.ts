export const locale = {
  lang: 'vi',
  data: {
    'Users': 'Người dùng',
    'Save': 'Lưu',
    'Add new user': 'Thêm người dùng',
    'User Detail' : 'Thông tin người dùng',
    'User type' : 'Loại người dùng',
    'Basic Info': 'Thông tin cơ bản',
    'Roles' : 'Vai trò',
    'Products': 'Chi nhánh',
    'Username': 'Tên đăng nhập',
    'Name': 'Tên',
    'Code': 'Mã nhân viên',
    'Email': 'Email',
    'Password': 'Mật khẩu',
    'Password (Confirm)': 'Nhập lại mật khẩu',
    'Active': 'Kích hoạt',
    'Actions': 'Thao tác',
    'Filter': 'Lọc',
    'Inactive': 'Huỷ bỏ',
    'All': 'Tất cả',
    'Consultant': 'Tư vấn',
    'Personal Trainer': 'HLV cá nhân (PT)',
    'Receptionist': 'Receptionist',
    'Trainer (Booking)': 'HLV (booking)',
    'Manager': 'Quản lý'
  }
};
