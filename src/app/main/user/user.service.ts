import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from '../base/base.service';
import { AuthService } from '../auth/auth.service';
import {AppConfig} from "../../app.config";
import {Observable} from "rxjs";

@Injectable()
export class UserService extends BaseService {
  public pathApi: string = '/profile';

  constructor(
    public httpClient: HttpClient,
    public authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public update(data): Observable<any> {
    return this.http.put(AppConfig.api_url + this.pathApi, data);
  }
}
