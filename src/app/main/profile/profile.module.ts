import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatDividerModule,
  MatIconModule,
  MatInputModule,
  MatSlideToggleModule, MatSnackBarModule,
  MatTabsModule
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '../../@fuse/shared.module';

import { ProfileComponent } from './profile.component';
import { ProfileAboutComponent } from './tabs/about/about.component';
import { ProfileRoutes } from './profile.routes';

@NgModule({
  declarations: [
    ProfileComponent,
    ProfileAboutComponent,
  ],
  imports: [
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatTabsModule,
    MatInputModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    TranslateModule,

    FuseSharedModule,

    ProfileRoutes
  ],
  providers: []
})
export class ProfileModule {
}
