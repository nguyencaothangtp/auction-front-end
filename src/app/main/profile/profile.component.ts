import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidators, PasswordValidators } from 'ngx-validators';
import { MatSnackBar } from '@angular/material';
import * as cloneDeep from 'clone-deep';

import { fuseAnimations } from '../../@fuse/animations';
import { FuseTranslationLoaderService } from '../../@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as vietnamese } from './i18n/vi';
import { AppState } from '../../app.service';
import { UserService } from '../user/user.service';
import {Router} from "@angular/router";

const languages = [ english, vietnamese ];

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ProfileComponent implements OnInit {
  public profileForm: FormGroup;
  public isLoading: boolean = false;

  constructor(
    public router: Router,
    public formBuilder: FormBuilder,
    public appState: AppState,
    public userService: UserService,
    public matSnackBar: MatSnackBar,
    public fuseTranslationLoaderService: FuseTranslationLoaderService
  ) {
    this.fuseTranslationLoaderService.loadTranslations(...languages);
  }

  public ngOnInit() {
    this.profileForm = this.buildForm();
  }

  public buildForm() {
    const form = this.formBuilder.group({
      username: [{value: this.appState.state.user.username, disabled: true}],
      full_name: [this.appState.state.user.full_name, [Validators.required]],
      phone_number: [this.appState.state.user.phone_number],
      maximum_bid_amount: [this.appState.state.user.maximum_bid_amount],
      password: [''],
      password_confirmation: [''],
      is_active: [this.appState.state.user.is_active || false, Validators.required]
    });

    form.setValidators(
      PasswordValidators.mismatchedPasswords('password', 'password_confirmation')
    );

    return form;
  }

  public async update() {
    this.isLoading = true;

    const response = await this.userService.update(this.prepareSubmitData(this.profileForm.value)).toPromise();

    this.isLoading = false;

    if (response) {
      // update appState user
      this.appState.state.user = response.data;
      localStorage.setItem('user', JSON.stringify(response));

      // Show the success message
      this.matSnackBar.open('Information saved', 'OK', {
        verticalPosition: 'top',
        duration: 2000,
      });
    }

    this.router.navigate(['/profile']);
  }

  public prepareSubmitData(value) {
    const data = cloneDeep(value);

    if (!data.password) {
      delete data.password;
    }

    delete data.password_confirmation;

    return data;
  }
}
