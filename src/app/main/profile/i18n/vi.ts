export const locale = {
  lang: 'vi',
  data: {
    'Save': 'Lưu',
    'Username': 'Tên đăng nhập',
    'Full Name': 'Họ Tên',
    'Maximum bid amount': 'Tối đa',
    'Password': 'Mật khẩu',
    'Password (Confirm)': 'Mật khẩu xác nhận',
    'Personal Info': 'Thông tin cá nhân'
  }
};
