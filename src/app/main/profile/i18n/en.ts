export const locale = {
  lang: 'en',
  data: {
    'Save': 'Save',
    'Username': 'Username',
    'Full Name': 'Full Name',
    'Maximum bid amount': 'Maximum bid amount',
    'Password': 'Password',
    'Password (Confirm)': 'Password (Confirm)',
    'Personal Info': 'Personal Info'
  }
};
