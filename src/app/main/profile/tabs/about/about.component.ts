import { Component, Input, OnInit } from '@angular/core';

import { fuseAnimations } from '../../../../@fuse/animations';

import { ProfileService } from 'app/main/pages/profile/profile.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'profile-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  animations: fuseAnimations
})
export class ProfileAboutComponent implements OnInit {
  @Input('form')
  public form: FormGroup;

  constructor(
  ) {
    // empty
  }

  public ngOnInit(): void {
    // empty
  }
}
