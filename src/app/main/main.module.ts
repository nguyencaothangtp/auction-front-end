import { NgModule } from '@angular/core';

import { MainRoutes } from './main.routes';

@NgModule({
  declarations: [
  ],
  imports: [
    MainRoutes,
  ]
})
export class MainModule {
}
