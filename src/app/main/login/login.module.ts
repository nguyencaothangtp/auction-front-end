import { NgModule } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { FuseSharedModule } from '../../@fuse/shared.module';

import { LoginRoutes } from './login.routes';
import { LoginComponent } from './login.component';
import { AuthModule } from '../auth/auth.module';
import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,

    FuseSharedModule,

    SharedModule,
    LoginRoutes,
    AuthModule
  ]
})
export class LoginModule {
}
