import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { fuseAnimations } from '../../@fuse/animations';
import { FuseConfigService } from '../../@fuse/services/config.service';

import { AppState } from '../../app.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: fuseAnimations
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public isLoading: boolean = false;

  constructor(
    public fuseConfigService: FuseConfigService,
    public formBuilder: FormBuilder,
    public router: Router,
    public appState: AppState,
    public authService: AuthService,
  ) {
    // Configure the layout
    this.fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  public ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', Validators.required]
    });
  }

  public async login() {
    this.isLoading = true;

    const response: any = await this.authService.login(this.loginForm.value);

    this.isLoading = false;

    if (response && response.user) {
      this.appState.state.user = response.user;
      localStorage.setItem('access_token', response.token);
      this.router.navigate(['/products']);
    }
  }
}
