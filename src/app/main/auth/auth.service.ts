import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConfig } from '../../app.config';
import { AppState } from '../../app.service';

@Injectable()
export class AuthService {
  constructor(
    public http: HttpClient,
    public appState: AppState
  ) {
    // empty
  }

  public login(data: any): Promise<any> {
    return this.http.post(AppConfig.api_url + '/login', data).toPromise();
  }

  public profile(): Promise<any> {
    return this.http.get(AppConfig.api_url + '/profile').toPromise();
  }

  public logout(): Promise<any> {
    return this.http.get(AppConfig.api_url + '/logout').toPromise();
  }

  public forgotPassword(data: any): Promise<any> {
    return this.http.post(AppConfig.api_url + '/reset-password', data).toPromise();
  }

  public getAccountByResetToken(token: string): Promise<any> {
    return this.http.get(AppConfig.api_url + '/reset-password/' + token).toPromise();
  }

  public resetPassword(data: any): Promise<any> {
    return this.http.put(AppConfig.api_url + '/reset-password', data).toPromise();
  }

  public isAccessible(permission): boolean {
    if (!this.appState.state.user || !this.appState.state.user.roles) {
      return false;
    }

    if (Array.isArray(permission)) {
      for (const element of permission) {
        if (this.isAccessible(element)) {
          return true;
        }
      }

      return false;
    }

    for (const role of this.appState.state.user.roles) {
      const permissions = role.permissions;

      const result = permissions.some((value) => {
        return value.name === permission;
      });

      if (result) {
        return true;
      }
    }

    return false;
  }

  public isBelongToBranch(branch): boolean {
    if (Array.isArray(branch)) {
      for (const element of branch) {
        if (this.isBelongToBranch(element)) {
          return true;
        }
      }

      return false;
    }

    if (!branch) {
      return false;
    }

    if (!this.appState.state.user || !this.appState.state.user.products) {
      return false;
    }

    return this.appState.state.user.products.some((value) => {
      return value.id === branch.id;
    });
  }
}
