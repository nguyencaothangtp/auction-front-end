import { NgModule } from '@angular/core';

import { AuthService } from './auth.service';

@NgModule({
  imports: [
  ],
  providers: [
    AuthService
  ],
  declarations: [
  ],
  exports: [
  ]
})

export class AuthModule {
}
