import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordValidators } from 'ngx-validators';
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '../../@fuse/animations';
import { FuseConfigService } from '../../@fuse/services/config.service';
import { AuthService } from '../auth/auth.service';
import { FuseSplashScreenService } from '../../@fuse/services/splash-screen.service';

@Component({
  selector: 'reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  animations: fuseAnimations
})
export class ResetPasswordComponent implements OnInit {
  public resetPasswordForm: FormGroup;

  public isLoading: boolean = false;
  public user: any = {};

  constructor(
    public fuseConfigService: FuseConfigService,
    public fuseSplashScreenService: FuseSplashScreenService,
    public formBuilder: FormBuilder,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public authService: AuthService,
    public toastrService: ToastrService
  ) {
    // Configure the layout
    this.fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  public async ngOnInit() {
    const token = this.activatedRoute.snapshot.params.token;

    this.buildForm(token);

    this.fuseSplashScreenService.waitResponse = true;

    this.user = await this.authService.getAccountByResetToken(token);

    this.fuseSplashScreenService.hide();

    if (!this.user) {
      this.router.navigate(['/']);
      return;
    }
  }

  public buildForm(token) {
    this.resetPasswordForm = this.formBuilder.group({
      password: ['', Validators.required],
      password_confirmation: ['', [Validators.required]],
      reset_token: [token, Validators.required]
    });

    this.resetPasswordForm.setValidators(
      PasswordValidators.mismatchedPasswords('password', 'password_confirmation')
    );
  }

  public async reset() {
    this.isLoading = true;

    const response = await this.authService.resetPassword(this.resetPasswordForm.value);

    this.isLoading = false;

    if (response) {
      this.toastrService.success('Password reset successfully. Sign in to continue.');
      this.router.navigate(['/']);
    }
  }
}
