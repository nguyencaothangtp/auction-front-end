import { NgModule } from '@angular/core';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { FuseSharedModule } from '../../@fuse/shared.module';

import { ResetPasswordRoutes } from './reset-password.routes';
import { ResetPasswordComponent } from './reset-password.component';
import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [
    ResetPasswordComponent
  ],
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,

    FuseSharedModule,

    ResetPasswordRoutes,
    SharedModule
  ]
})
export class ResetPasswordModule {
}
