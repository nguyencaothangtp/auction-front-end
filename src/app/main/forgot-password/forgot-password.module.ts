import { NgModule } from '@angular/core';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { FuseSharedModule } from '../../@fuse/shared.module';

import { ForgotPasswordComponent } from './forgot-password.component';
import { ForgotPasswordRoutes } from './forgot-password.routes';
import { SharedModule } from '../shared.module';

@NgModule({
  declarations: [
    ForgotPasswordComponent
  ],
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,

    FuseSharedModule,

    SharedModule,
    ForgotPasswordRoutes
  ]
})
export class ForgotPasswordModule {
}
