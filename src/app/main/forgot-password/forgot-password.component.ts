import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '../../@fuse/animations';
import { FuseConfigService } from '../../@fuse/services/config.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: [ './forgot-password.component.scss' ],
  animations: fuseAnimations
})
export class ForgotPasswordComponent implements OnInit {
  public forgotPasswordForm: FormGroup;

  public isLoading: boolean = false;

  constructor(
    public fuseConfigService: FuseConfigService,
    public formBuilder: FormBuilder,
    public authService: AuthService,
    public toastrService: ToastrService
  ) {
    // Configure the layout
    this.fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  public ngOnInit(): void {
    this.forgotPasswordForm = this.formBuilder.group({
      email: [ '', [ Validators.required, Validators.email ] ]
    });
  }

  public async forgot() {
    this.isLoading = true;

    const response = await this.authService.forgotPassword(this.forgotPasswordForm.value);

    this.isLoading = false;

    if (response && response.code === 200) {
      this.toastrService.success(response.message);
    }
  }
}
