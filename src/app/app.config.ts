export const AppConfig = {
  api_url: process.env.API_URL,
  image_default: 'assets/img/default-image.png',
};

export const Colors = {
  active: 'mat-green-500-bg',
  inactive: 'mat-red-900-bg',
};
