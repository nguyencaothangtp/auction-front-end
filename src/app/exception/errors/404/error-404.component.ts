import { Component } from '@angular/core';
import { Location } from '@angular/common';

import { FuseConfigService } from '../../../@fuse/services/config.service';

@Component({
  selector: 'error-404',
  templateUrl: './error-404.component.html',
  styleUrls: [ './error-404.component.scss' ]
})
export class Error404Component {
  constructor(
    public fuseConfigService: FuseConfigService,
    public location: Location
  ) {
    // Configure the layout
    this.fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  public back() {
    this.location.back();
  }
}
