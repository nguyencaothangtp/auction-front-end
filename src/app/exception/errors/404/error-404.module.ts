import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material';

import { FuseSharedModule } from '../../../@fuse/shared.module';

import { Error404Routes } from './error-404.routes';
import { Error404Component } from './error-404.component';

@NgModule({
  declarations: [
    Error404Component
  ],
  imports: [
    Error404Routes,

    MatIconModule,

    FuseSharedModule
  ]
})
export class Error404Module {
}
