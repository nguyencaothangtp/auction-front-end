import { Router } from '@angular/router';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { AppInjector } from '../app.injector';
import { AppState } from '../app.service';

export class HttpErrorInterceptor implements HttpInterceptor {

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((e: HttpErrorResponse) => {
          const toastrService = AppInjector.get(ToastrService);
          const appState = AppInjector.get(AppState);
          const router = AppInjector.get(Router);

          let errorMessage = '';

          if (e.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = e.error.message;

          } else if (e.error && e.error.expired) {
            errorMessage = e.error.error;

            localStorage.removeItem('access_token');
            localStorage.removeItem('user');
            appState.state.user = null;

            router.navigate(['/login']);

          } else if (e.error) {
            // error defined
            errorMessage = e.error.message;
            toastrService.error(errorMessage);
            return new Observable<any>((subscriber) => subscriber.complete());

          } else {
            // server-side error
            errorMessage = `Error Code: ${e.error} <br> Message: ${e.message}`;
          }

          toastrService.error(errorMessage);

          return throwError(errorMessage);
        })
      );
  }
}
