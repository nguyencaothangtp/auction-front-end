import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';

import { FusePerfectScrollbarDirective } from '../../../../../@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { FuseConfigService } from '../../../../../@fuse/services/config.service';
import { FuseNavigationService } from '../../../../../@fuse/components/navigation/navigation.service';
import { FuseSidebarService } from '../../../../../@fuse/components/sidebar/sidebar.service';

import { AppState } from '../../../../../app.service';

@Component({
  selector: 'navbar-vertical-style-1',
  templateUrl: './style-1.component.html',
  styleUrls: ['./style-1.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarVerticalStyle1Component implements OnInit, OnDestroy {
  public fuseConfig: any;
  public fusePerfectScrollbarUpdateTimeout: any;
  public navigation: any;

  public fusePerfectScrollbar: FusePerfectScrollbarDirective;
  public unsubscribeAll: Subject<any>;

  constructor(
    public fuseConfigService: FuseConfigService,
    public fuseNavigationService: FuseNavigationService,
    public fuseSidebarService: FuseSidebarService,
    public router: Router,
    public appState: AppState
  ) {
    // Set the public defaults
    this.unsubscribeAll = new Subject();
  }

  // Directive
  @ViewChild(FusePerfectScrollbarDirective)
  set directive(theDirective: FusePerfectScrollbarDirective) {
    if (!theDirective) {
      return;
    }

    this.fusePerfectScrollbar = theDirective;

    // Update the scrollbar on collapsable item toggle
    this.fuseNavigationService.onItemCollapseToggled
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(() => {
        this.fusePerfectScrollbarUpdateTimeout = setTimeout(() => {
          this.fusePerfectScrollbar.update();
        }, 310);
      });

    // Scroll to the active item position
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        take(1)
      )
      .subscribe(() => {
          setTimeout(() => {
            const activeNavItem: any = document.querySelector('navbar .nav-link.active');

            if (activeNavItem) {
              const activeItemOffsetTop = activeNavItem.offsetTop;
              const activeItemOffsetParentTop = activeNavItem.offsetParent.offsetTop;
              const scrollDistance = activeItemOffsetTop - activeItemOffsetParentTop - (48 * 3) - 168;

              this.fusePerfectScrollbar.scrollToTop(scrollDistance);
            }
          });
        }
      );
  }

  /**
   * On init
   */
  public ngOnInit(): void {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        takeUntil(this.unsubscribeAll)
      )
      .subscribe(() => {
          if (this.fuseSidebarService.getSidebar('navbar')) {
            this.fuseSidebarService.getSidebar('navbar').close();
          }
        }
      );

    // Subscribe to the config changes
    this.fuseConfigService.config
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((config) => {
        this.fuseConfig = config;
      });

    // Get current navigation
    this.fuseNavigationService.onNavigationChanged
      .pipe(
        filter((value) => value !== null),
        takeUntil(this.unsubscribeAll)
      )
      .subscribe(() => {
        this.navigation = this.fuseNavigationService.getCurrentNavigation();
      });
  }

  /**
   * On destroy
   */
  public ngOnDestroy(): void {
    if (this.fusePerfectScrollbarUpdateTimeout) {
      clearTimeout(this.fusePerfectScrollbarUpdateTimeout);
    }

    // Unsubscribe from all subscriptions
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  /**
   * Toggle sidebar opened status
   */
  public toggleSidebarOpened(): void {
    this.fuseSidebarService.getSidebar('navbar').toggleOpen();
  }

  /**
   * Toggle sidebar folded status
   */
  public toggleSidebarFolded(): void {
    this.fuseSidebarService.getSidebar('navbar').toggleFold();
  }
}
