import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';

import { FuseConfigService } from '../../../@fuse/services/config.service';
import { FuseSidebarService } from '../../../@fuse/components/sidebar/sidebar.service';

import { locale as english } from './i18n/en';
import { locale as vietnamese } from './i18n/vi';
import { navigation } from 'app/navigation/navigation';
import { AuthService } from '../../../main/auth/auth.service';
import { AppState } from '../../../app.service';
import { FuseTranslationLoaderService } from '../../../@fuse/services/translation-loader.service';

const languages = [ english, vietnamese ];

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: [ './toolbar.component.scss' ]
})

export class ToolbarComponent implements OnInit, OnDestroy {
  public horizontalNavbar: boolean;
  public rightNavbar: boolean;
  public hiddenNavbar: boolean;
  public languages: any;
  public navigation: any;
  public selectedLanguage: any;
  public userStatusOptions: any[];

  public _unsubscribeAll: Subject<any>;

  constructor(
    public authService: AuthService,
    public router: Router,
    public appState: AppState,
    public fuseConfigService: FuseConfigService,
    public fuseSidebarService: FuseSidebarService,
    public translateService: TranslateService,
    public fuseTranslationLoaderService: FuseTranslationLoaderService
  ) {
    // Set the defaults
    this.userStatusOptions = [
      {
        title: 'Online',
        icon: 'icon-checkbox-marked-circle',
        color: '#4CAF50'
      },
      {
        title: 'Away',
        icon: 'icon-clock',
        color: '#FFC107'
      },
      {
        title: 'Do not Disturb',
        icon: 'icon-minus-circle',
        color: '#F44336'
      },
      {
        title: 'Invisible',
        icon: 'icon-checkbox-blank-circle-outline',
        color: '#BDBDBD'
      },
      {
        title: 'Offline',
        icon: 'icon-checkbox-blank-circle-outline',
        color: '#616161'
      }
    ];

    this.languages = [
      {
        id: 'en',
        title: 'English',
        flag: 'us'
      },
      {
        id: 'vi',
        title: 'Vietnamese',
        flag: 'vi'
      }
    ];

    this.navigation = navigation;

    this.fuseTranslationLoaderService.loadTranslations(...languages);

    // Set the public defaults
    this._unsubscribeAll = new Subject();
  }

  public ngOnInit(): void {
    // Subscribe to the config changes
    this.fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((settings) => {
        this.horizontalNavbar = settings.layout.navbar.position === 'top';
        this.rightNavbar = settings.layout.navbar.position === 'right';
        this.hiddenNavbar = settings.layout.navbar.hidden === true;
      });

    // Set the selected language from default languages
    this.selectedLanguage = _.find(this.languages, { id: this.translateService.currentLang });
  }

  public ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  public toggleSidebarOpen(key): void {
    this.fuseSidebarService.getSidebar(key).toggleOpen();
  }

  public search(value): void {
    // Do your search here...
    console.log(value);
  }

  public setLanguage(lang): void {
    // Set the selected language for the toolbar
    this.selectedLanguage = lang;

    // Use the selected language for translations
    this.translateService.use(lang.id);

    localStorage.setItem('currentLanguage', lang.id);
  }

  public async logout() {
    const response = await this.authService.logout();

    if (response) {
      localStorage.removeItem('access_token');
      localStorage.removeItem('user');
      this.appState.state.user = null;

      this.router.navigate(['/login']);
    }
  }

  public moveTo(path) {
    this.router.navigate([path]);
  }
}
